/*
 * Turn canvas elements with gle class into live glsl renderers
 */
var GLE;
(function (GLE) {
    var Gle = /** @class */ (function () {
        function Gle(el) {
            this.el = el;
            this.animate = false;
            this.gl = null;
            this.buffer = null;
            this.quad = null;
            this.atr_vpos = null;
            this.quad_itemSize = null;
            this.quad_numItems = null;
            this.start = null;
        }
        Gle.prototype.setup_context = function () {
            var el = this.el;
            var gl = this.gl;
            gl.viewport(0, 0, this.el.width, this.el.height);
            var vertexSource = "attribute vec4 vpos;\nvarying highp vec2 pv;\nvoid main(void) {\ngl_Position = vec4(vpos.xy*1000.0-500.0,vpos.z,1.0);\npv = vpos.xy;\n}";
            var vertexShader = gl.createShader(gl.VERTEX_SHADER);
            gl.shaderSource(vertexShader, vertexSource);
            gl.compileShader(vertexShader);
            if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
                console.log("vertex shader compile error:", gl.getShaderInfoLog(vertexShader));
            }
            var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
            var fragmentSource = el.textContent;
            // Prepend default parts
            fragmentSource = "precision highp float;\nuniform float time;\nuniform vec2 size;" + fragmentSource;
            gl.shaderSource(fragmentShader, fragmentSource);
            gl.compileShader(fragmentShader);
            if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
                console.log("fragment shader compiler error:", gl.getShaderInfoLog(fragmentShader));
            }
            var shaderProgram = gl.createProgram();
            gl.attachShader(shaderProgram, vertexShader);
            gl.attachShader(shaderProgram, fragmentShader);
            gl.linkProgram(shaderProgram);
            if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
                alert("Problem with shaders");
            }
            gl.useProgram(shaderProgram);
            this.atr_vpos = gl.getAttribLocation(shaderProgram, "vpos");
            gl.enableVertexAttribArray(this.atr_vpos);
            var quad = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, quad);
            var vertices = [1.0, 1.0, 0.0, -1.0, 1.0, 0.0, 1.0, -1.0, 0.0, -1.0, -1.0, 0.0];
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
            this.quad_itemSize = 3;
            this.quad_numItems = 4;
            this.uni_time = gl.getUniformLocation(shaderProgram, "time");
            this.uni_size = gl.getUniformLocation(shaderProgram, "size");
            // Preserve values
            this.quad = quad;
            this.shaderProgram = shaderProgram;
        };
        Gle.prototype.create_context = function () {
            var el = this.el;
            el.addEventListener('webglcontextcreationerror', glcontexterror);
            el.addEventListener('webglcontextlost', glcontextlost);
            var attrs = {
                preserveDrawingBuffer: !this.animate,
                premultipliedAlpha: true
            };
            var gl = (el.getContext("webgl", attrs) || el.getContext("experimental-webgl", attrs));
            if (!gl)
                return false; // No WebGL
            created_contexts++;
            this.gl = gl;
            el.style.opacity = "1"; // Make visible
            return true;
        };
        Gle.prototype.release_context = function () {
            var el = this.el;
            var newcanv = document.createElement('canvas');
            newcanv.width = el.width;
            newcanv.height = el.height;
            newcanv.style.cssText = el.style.cssText;
            newcanv.className = el.className;
            newcanv.textContent = el.textContent;
            newcanv.id = el.id;
            newcanv.setAttribute("animate", el.getAttribute("animate"));
            el.parentNode.replaceChild(newcanv, el); // Swap with old one so we can create a new context
            this.el = newcanv;
            delete this.gl;
            delete this.quad;
            delete this.shaderProgram;
        };
        Gle.prototype.render = function (time) {
            var el = this.el;
            var gl = this.gl;
            // Set clear color to black, fully opaque
            gl.clearColor(0.0, 0.0, 0.0, 0.0);
            gl.blendFunc(gl.ONE, gl.ONE);
            gl.enable(gl.BLEND);
            gl.clear(gl.COLOR_BUFFER_BIT);
            gl.bindBuffer(gl.ARRAY_BUFFER, this.quad);
            gl.vertexAttribPointer(this.atr_vpos, this.quad_itemSize, gl.FLOAT, false, 0, 0);
            gl.uniform1f(this.uni_time, time);
            gl.uniform2f(this.uni_size, el.width, el.height);
            gl.drawArrays(gl.TRIANGLE_STRIP, 0, this.quad_numItems);
            gl.flush();
            //if (!el.animate) {
            if (this.buffer == null) {
                this.buffer = new Uint8Array(el.width * el.height * 4);
                gl.readPixels(0, 0, el.width, el.height, gl.RGBA, gl.UNSIGNED_BYTE, this.buffer);
            }
        };
        Gle.prototype.do_animate = function (timestamp) {
            if (!this.start) {
                this.start = timestamp;
            }
            if (this.gl == null || this.gl.isContextLost()) {
                this.release_context();
                if (!this.create_context())
                    return;
                this.setup_context();
            }
            this.render(timestamp - this.start);
        };
        Gle.prototype.show_buffer = function () {
            // Assume gle is unused canvase
            var ctx2d = this.el.getContext('2d', { alpha: true });
            var idata = ctx2d.createImageData(this.el.width, this.el.height);
            idata.data.set(this.buffer);
            ctx2d.putImageData(idata, 0, 0);
            this.el.style.opacity = "1";
        };
        return Gle;
    }());
    var gles = []; // TODO: Make class for this
    var animating = false;
    var animation_handle = null;
    var hit_max_contexts = false; // After first context lost, trigger this
    var created_contexts = 0; // Current estimate of how many contexts we have made
    function convert_to_static() {
        var count = gles.length;
        for (var k = 0; k < count; k++) {
            var el = gles[k].el;
            var gl = gles[k].gl;
            var buffer = gles[k].buffer;
            if (!gles[k].animate && gl.isContextLost()) {
                // Try to get a 2d context instead
                var canv2d = document.createElement('canvas');
                canv2d.width = el.width;
                canv2d.height = el.height;
                canv2d.style.cssText = el.style.cssText;
                canv2d.className = el.className;
                el.parentNode.replaceChild(canv2d, el);
                var ctx = canv2d.getContext('2d', { alpha: true });
                var idata = ctx.createImageData(canv2d.width, canv2d.height);
                idata.data.set(buffer);
                ctx.putImageData(idata, 0, 0);
            }
        }
    }
    function setup_animation() {
        if (document.hidden) {
            if (animation_handle != null) {
                console.log("Cancelling animation");
                window.cancelAnimationFrame(animation_handle);
                animation_handle = null;
            }
        }
        else {
            if (animating && (animation_handle == null)) {
                animation_handle = window.requestAnimationFrame(animate_all);
            }
        }
    }
    function element_in_view(el) {
        var elpos = el.getBoundingClientRect();
        var bodypos = document.body.getBoundingClientRect();
        if ((elpos.width == 0) || (elpos.height == 0))
            return false;
        var eltop = elpos.top - bodypos.top;
        var elbot = elpos.bottom - bodypos.top;
        var wintop = window.pageYOffset;
        var winbot = window.pageYOffset + window.innerHeight;
        if ((elbot < wintop) ||
            (eltop > winbot)) {
            return false;
        }
        else
            return true;
    }
    function get_visible() {
        var count = gles.length;
        var visible = [];
        created_contexts = 0; // Now we will recalculate
        for (var k = 0; k < count; k++) {
            var gle = gles[k];
            if (gle.gl != null && !gle.gl.isContextLost()) {
                created_contexts++;
            }
            if (element_in_view(gle.el)) {
                visible.push(k);
            }
        }
        return visible;
    }
    function draw_all(timestamp) {
        var visible = get_visible();
        var count = visible.length;
        var available = created_contexts;
        var limit_contexts = hit_max_contexts;
        for (var v = 0; v < count; v++) {
            var k = visible[v];
            var gle = gles[k];
            if (limit_contexts) {
                if (available > 0) {
                    gle.do_animate(timestamp);
                    available--;
                }
                else {
                    // Try to render any remaining elements with cached buffers
                    if (gle.buffer != null) {
                        gle.release_context();
                        gle.show_buffer();
                    }
                }
            }
            else {
                gles[k].do_animate(timestamp);
            }
        }
    }
    function animate_all(timestamp) {
        animation_handle = null;
        draw_all(timestamp * 0.001); // Convert from milliseconds to seconds
        setup_animation();
    }
    function scroll_event(e) {
        // Used when not animating. Draw all visible elements
        draw_all(0.0);
    }
    function glcontexterror(e) {
        //console.log("GL context creation error",e);
    }
    function glcontextlost(e) {
        hit_max_contexts = true;
        //console.log("GL context lost",e);
        //e.preventDefault(); <-- should only do this if implementing restore
    }
    function visibility_changed() {
        console.log("Window visibility change", document.hidden);
        setup_animation();
    }
    function init_gle() {
        // Watch for visbility change
        if (typeof document.hidden !== "undefined") {
            document.addEventListener("visibilitychange", visibility_changed, false);
        }
        var candidates = document.getElementsByClassName("gle");
        var count = candidates.length;
        console.log("Found", count, "GLE elements");
        for (var k = 0; k < count; k++) {
            var el = candidates[k];
            var canvel = null;
            if (el instanceof HTMLCanvasElement)
                canvel = el;
            if (!canvel)
                continue;
            var gle = new Gle(canvel);
            if (el.hasAttribute("animate")) {
                gle.animate = true;
                animating = true; // Need to run animation loop
            }
            //if (!create_context(gle)) return;
            //setup_context(gle);
            //render(gle, 0.0); // Starting time
            //el.style.opacity = 1;
            gles[k] = gle;
        }
        setup_animation();
        if (!animating) {
            draw_all(0.0); // Make sure any visible elements drawn before first scroll event
            document.addEventListener("scroll", scroll_event, false);
        }
        // convert_to_static();
    }
    // Initialisation 
    var next_onload = window.onload;
    window.onload = function () {
        init_gle();
        // Run any previously queued start function
        if (next_onload)
            next_onload.call(window);
    };
})(GLE || (GLE = {}));

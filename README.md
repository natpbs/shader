[![pipeline status](https://gitlab.com/natpbs/shader/badges/master/pipeline.svg)](https://gitlab.com/natpbs/shader/-/commits/master)

# natpbs/shader

A repository for my shading experiments. You can visit the resulting website on gitlab [pages]
I also publish as natpbs on [shaderoy].

[pages]: https://natpbs.gitlab.io/shader/
[shadertoy]: https://shadertoy.org/user/natpbs/ "shadertoy"
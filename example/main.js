function main() {
  const root = document.querySelector("html");
  const canvas = document.getElementById("canvas");
  
  // resize
  function resizeHandler(){
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;
  }
  
  window.addEventListener("resize", resizeHandler);
  
  resizeHandler();
  
  // fullscreen
  const isFullscreenSpan = document.getElementById("isFullscreen");
  function fullscreenChangeHandler() {
    //alert(1);
    const isFullscreen = document.fullscreenElement;
    isFullscreenSpan.innerHTML = isFullscreen ? "exit" : "enter";
  }
  
  document.addEventListener("fullscreenchange", fullscreenChangeHandler);
  
  function fullscreenHandler() {
    const isFullscreen = document.fullscreenElement;
    isFullscreenSpan.innerHTML = "changing";
    const whenChanged = isFullscreen ? document.exitFullscreen() : document.body.requestFullscreen();
  }
  
  const fullscreenButton = document.querySelector("button.fullscreen");
  
  fullscreenButton.onclick = fullscreenHandler;
  
  // Initialize the GL context
  var gl = canvas.getContext("webgl", { depth: false });

  // Only continue if WebGL is available and working
  if (gl === null) {
    alert("Unable to initialize WebGL. Your browser or machine may not support it.");
    return;
  }

  // Set clear color to black, fully opaque
  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  // Clear the color buffer with specified clear color
  gl.clear(gl.COLOR_BUFFER_BIT);

  // fragment shader
  const fragment = gl.createShader(gl.FRAGMENT_SHADER);
  const fragmentSource = document.getElementById("fragment").text;
  gl.shaderSource(fragment, fragmentSource);
  gl.compileShader(fragment);
  if (!gl.getShaderParameter(fragment, gl.COMPILE_STATUS)) {
    throw new Error(gl.getShaderInfoLog(fragment));
  }

  // vertex
  const vertex = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(vertex, `
  attribute vec2 a_vertex;
  void main(void) {
    gl_Position = vec4(a_vertex, 0.0, 1.0);
  }
  `);
  gl.compileShader(vertex);
  if (!gl.getShaderParameter(vertex, gl.COMPILE_STATUS)) {
    throw new Error(gl.getShaderInfoLog(vertex));
  }

  // program
  const program = gl.createProgram();
  gl.attachShader(program, vertex);
  gl.attachShader(program, fragment);
  gl.linkProgram(program);
  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    throw new Error(gl.getProgramInfoLog(program));
  }

  // buffer
  const buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.bufferData(gl.ARRAY_BUFFER, Float32Array.of(-1, -1, +1, -1, +1, +1, -1, +1), gl.STATIC_DRAW);

  // setup
  const a_vertex = gl.getAttribLocation(program, "a_vertex");
  gl.useProgram(program);
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.enableVertexAttribArray(a_vertex);
  gl.vertexAttribPointer(a_vertex, 2, gl.FLOAT, false, 0, 0);

  // uniforms
  uniforms = {
    resolution: gl.getUniformLocation(program, "resolution"),
    ftime: gl.getUniformLocation(program, "ftime"),
    touch: gl.getUniformLocation(program, "touch")
  };

  let touch = { x: 0, y: 0 };

  function mouseMoveHandler(event) {
    // event.preventDefault();
    touch.x = event.offsetX;
    touch.y = canvas.height - event.offsetY;
  }

  function touchMoveHandler(event) {
    // event.preventDefault();
    const { clientX, clientY } = event.touches[0];
    touch.x = clientX;
    touch.y = canvas.height - clientY;
  }
  canvas.addEventListener("mousemove", mouseMoveHandler, false);
  canvas.addEventListener("touchmove", touchMoveHandler, false);

  // ftime_period
  const period_definition = /\n\s*\#\s*define\s+FTIME_PERIOD\s+(\d+)\s*/;
  const period_match = fragmentSource.match(period_definition);
  const ftime_period = period_match ? Number(period_match[1]) : 6000;
  console.log("ftime_period: ", ftime_period, "\nmodified: ", !!period_match);

  // draw

  let frame = 0;
  let mtime_prev = 0;
  function log_fps(frame, mtime){
    const sample_size = 128;
    if (frame % sample_size == 0) {
      const fps = 1000 * sample_size / (mtime - mtime_prev);
      console.log("fps: ", fps);
      mtime_prev = mtime;
    }
  }

  function draw(mtime) {
    //log_fps(frame, mtime);
    let ftime = (mtime % ftime_period) / ftime_period;
    ftime = -1 + 2 * ftime;
    const {width, height} = canvas;
    gl.viewport(0, 0, width, height);
    gl.uniform2f(uniforms.resolution, width, height);
    gl.uniform1f(uniforms.ftime, ftime);
    gl.uniform2f(uniforms.touch, touch.x, touch.y);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    requestAnimationFrame(draw);
    frame++;
  }

  draw(0);
  document.body.style.opacity = 1;
}

window.onload = main;